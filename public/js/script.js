const details = {
    username: "Person1",
    password: "123"
}

var movies;
var favourites;
var favmovie;
var isLoggedIn = false;


function getMovies() {

	var xhr = new XMLHttpRequest();
	xhr.open("get", "http://localhost:3000/movies");

	xhr.getResponseHeader("Content-Type", "application/json")

	xhr.send(null)

	xhr.onload = function () {

		movies = JSON.parse(xhr.responseText);

		let str = "";

		movies.forEach(function (movie) {

			str += `<div class="card mb-3" style="max-width: 540px;">
			<div class="row g-0">
			  <div class="col-md-4">
				<img src="${movie.image}" class="img-fluid rounded-start" alt="...">
			  </div>
			  <div class="col-md-8">
				<div class="card-body">
				  <h5 class="card-title">${movie.title}</h5>
				  <p class="card-text">${movie.description}</p>
				  <p class="card-text"><small class="text-muted">${movie.year}</small></p>
				  <button class="btn btn-primary" onclick="addFavourite(${movie.id})">Add To Favourites</button>
				</div>
			  </div>
			</div>
		  </div>`

		// 	str += `<div class="card" style="width: 18rem;">
		// 	<img src="${movie.image}" class="card-img-top" alt="..." style = "width: 100px">
		// 	<div class="card-body">
		// 	  <h5 class="card-title">${movie.title}</h5>
		// 	  <p class="card-text">${movie.description}</p>
		// 	  <button class="btn btn-primary" onclick="addFavourite(${movie.id})">Add To Favourites</button>
		// 	</div>
		//   </div>`

// 			str += `<li>${movie.title} </li>

//             <img src=${movie.image} height=200px>

//             <button onclick="addFavourite(${movie.id})">Add To Favourites</button>`

		});

		document.getElementById("moviesList").innerHTML = str;

	}




}




function getFavourites() {

	var xhr = new XMLHttpRequest();




	xhr.open("GET", "http://localhost:3000/favourites");

	xhr.getResponseHeader("Content-Type", "application/json")

	xhr.send(null)

	xhr.onload = function () {

		favourites = JSON.parse(xhr.responseText);

		let str = "";

		favourites.forEach(function (favmovie) {

			str += `<div class="card mb-3" style="max-width: 540px;">
			<div class="row g-0">
			  <div class="col-md-4">
				<img src="${favmovie.image}" class="img-fluid rounded-start" alt="...">
			  </div>
			  <div class="col-md-8">
				<div class="card-body">
				  <h5 class="card-title">${favmovie.title}</h5>
				  <p class="card-text">${favmovie.description}</p>
				  <p class="card-text"><small class="text-muted">${favmovie.year}</small></p>
				  <button class="btn btn-primary" type="button" onclick="removeFavourite(${favmovie.id})">Remove</button>
				</div>
			  </div>
			</div>
		  </div>`

		// 	str += `<div class="card" style="width: 18rem;">
		// 	<img src="${favmovie.image}" class="card-img-top" alt="..." style = "width: 100px">
		// 	<div class="card-body">
		// 	  <h5 class="card-title">${favmovie.title}</h5>
		// 	  <p class="card-text">${favmovie.description}</p>
		// 	  <button class="btn btn-primary" onclick="removeFavourite(${favmovie.id})">Remove</button>
		// 	</div>
		//   </div>`

// 			str += `<li>${favmovie.title} </li><img src=${favmovie.image} height=200px>

//         <button onclick="removeFavourite(${favmovie.id})">Remove</button>`

		});

		document.getElementById("favouritesList").innerHTML = str;

	}

}

var duplicate = false;

function addFavourite(movie_id) {

	movies.forEach(function (movie) {

		if (movie.id === movie_id) {

			favmovie = movie;

		}

	});

	favourites.forEach((fav) => {

		if (fav.id === movie_id) {

			alert("Already Added To Favourites.");

			duplicate = true;

		}

	});

	if (!duplicate) {

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "http://localhost:3000/favourites");
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.send(JSON.stringify(favmovie))

	}

}




function removeFavourite(movie_id) {

	var xhr = new XMLHttpRequest();
	xhr.open("DELETE", "http://localhost:3000/favourites/" + String(movie_id));
	xhr.send(null)

}

// //checking
// function check(){
//     // var storedName = localStorage.getItem('name');
//     // var storedPw = localStorage.getItem('pw');

//     var userName = document.getElementById('userName');
//     var userPw = document.getElementById('userPw');
//     // var userRemember = document.getElementById("rememberMe");
//     console.log(details.username, userName);

//     if(details.username === userName.value && details.password === userPw.value){
//         //alert('You are logged in.');
//         isLoggedIn = true;
// 		window.location.replace("http://stackoverflow.com");

// 		//document.getElementsByTagName("h1").innerHTML = Window.location;
// 		//console.log(document.getElementsByTagName("h1"));
//     }else{
//         alert('Error on login');
//         //console.log(details);
//     }
//     //alert(`${isLoggedIn}`);
// }



getMovies();

getFavourites();


// You will get error - Uncaught ReferenceError: module is not defined
// while running this script on browser which you shall ignore
// as this is required for testing purposes and shall not hinder
// it's normal execution


